Fixelpuckers
============

Fixelpuckers is a community app that's designed show off UI's for different OS'. It is created using Ruby on Rails.

Contributing
============

This is going to be a large project so any help is appreciated. I ask that any submitted coded come with proper tests.

Questions
=========

For questions come find me in the #pixelfuckers channel on Freenode. I'll do my best to help with any questions you may have.
